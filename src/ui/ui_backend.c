/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\ui\ui_backend.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\ui
 * Created Date: Wednesday, July 31st 2019, 9:00:07 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "ui_backend.h"
#include "gl_batcher.h"

image char_texture;
vec3 text_color;

void DrawQuad(float x, float y, float w, float h, float u, float v, float uw, float vh);
void DrawQuad_NoTex(float x, float y, float w, float h);

void UI_Init(void)
{
    char_texture = IMG_load("char.png");
    RNDR_CreateTextureFromImage(&char_texture);
    glm_vec3_copy(GLM_VEC3_ONE, text_color);
}

extern int text_size;

void UI_DrawCharacter(int x, int y, int num)
{
    int row, col;
    float frow, fcol, size;

    if (num == 32)
        return; // space

    num &= 255;

    if (y <= -8)
        return; // totally off screen

    row = num >> 4;
    col = num & 15;

    frow = row * 0.0625;
    fcol = col * 0.0625;
    size = 0.0625;

    GL_Bind(char_texture);
	DrawQuad(x, y, text_size, text_size, fcol, frow, size, size);
}

void UI_DrawString(int x, int y, char *str)
{
    GL_Bind(char_texture);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    R_BeginBatchingSurfacesQuadWithColor(text_color);
    int row, col;
    float frow, fcol, size;
    unsigned char num;
    while (*str)
    {
        num = (unsigned char)*str & 255;
        if (num == 32)
        {
            x += text_size;
            str++;
            continue;
        }

        row = num >> 4;
        col = num & 15;
        frow = row * 0.0625f;
        fcol = col * 0.0625f;
        size = 0.0625f;

        R_BatchSurfaceQuadText(x, y, frow, fcol, size);
        x += text_size;
        str++;
    }
    R_EndBatchingSurfacesQuads();
}

void UI_TextColor(float r, float g, float b)
{
    text_color[0] = r;
    text_color[1] = g;
    text_color[2] = b;
}

static void Draw_AlphaPic(int x, int y, image *pic, float alpha)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1, 1, 1, alpha);
    //GL_Bind(gl->texnum);
    GL_Bind(*pic);
    //DrawQuad(x, y, pic->width, pic->height, gl->sl, gl->tl, gl->sh - gl->sl, gl->th - gl->tl);
    DrawQuad(x, y, pic->width, pic->height, 0,0,1,1);
    glColor4f(1, 1, 1, 1);
    glDisable(GL_BLEND);
}

void UI_DrawPic(int x, int y, image *pic)
{
    glColor4f(1, 1, 1, 1);
    GL_Bind(*pic);
    //DrawQuad(x, y, pic->width, pic->height, gl->sl, gl->tl, gl->sh - gl->sl, gl->th - gl->tl);
    DrawQuad(x, y, pic->width, pic->height, 0,0,1,1);
}

void UI_DrawTransPic(int x, int y, image *pic)
{
    Draw_AlphaPic(x, y, pic, 1.0f);
}

void UI_DrawFill(int x, int y, int w, int h, int r, int g, int b)
{
    glDisable(GL_TEXTURE_2D);
    glColor3f(r / 255.0f, g / 255.0f, b / 255.0f);

    DrawQuad_NoTex(x, y, w, h);

    glColor3f(1, 1, 1);
    glEnable(GL_TEXTURE_2D);
}

void Draw_FadeScreen(void)
{
#if 0
    glDisable(GL_ALPHA_TEST);
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);
#define OPACITY 0.6f

    float vertex[(3 + 4) * 4] = {//xyz,rgba
                                 0, 0, 0, 0.0f, 0.0f, 0.0f, OPACITY,
                                 0, vid.height, 0, 0.0f, 0.0f, 0.0f, OPACITY,
                                 vid.width, 0, 0, 0.0f, 0.0f, 0.0f, OPACITY,
                                 vid.width, vid.height, 0, 0.0f, 0.0f, 0.0f, OPACITY};

    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glVertexPointer(3, GL_FLOAT, 7 * sizeof(GLfloat), vertex);
    glColorPointer(4, GL_FLOAT, 7 * sizeof(GLfloat), vertex + 3);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glDisableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glDisable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_ALPHA_TEST);
#endif
}

void UI_Set2D(void)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 640, 480, 0, -1, 1);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void DrawQuad_NoTex(float x, float y, float w, float h)
{
    float vertex[6 * 3] = {x, y, 0, x + w, y, 0, x, y + h, 0, x, y + h, 0, x + w, y, 0, x + w, y + h, 0};
    glVertexPointer(3, GL_FLOAT, 0, vertex);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}

void DrawQuad(float x, float y, float w, float h, float u, float v, float uw, float vh)
{
    float vertex[6 * 3] = {x, y, 0, x + w, y, 0, x, y + h, 0, x, y + h, 0, x + w, y, 0, x + w, y + h, 0};
    float texcoord[6 * 2] = {u, v, u + uw, v, u, v + vh, u, v + vh, u + uw, v, u + uw, v + vh};
    glVertexPointer(3, GL_FLOAT, 0, vertex);
    glTexCoordPointer(2, GL_FLOAT, 0, texcoord);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}