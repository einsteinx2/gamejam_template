#ifndef IMAGE_LOADER_H
#define IMAGE_LOADER_H

#include "common.h"

/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\image_loader.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, June 29th 2019, 9:17:14 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

typedef struct image
{
    char name[16];
    int width, height;
    int format;
    int channels;
    unsigned char *data;
    unsigned int id;
} image;

image IMG_load(char *path);
void IMG_unload(image *img);

#endif /* IMAGE_LOADER_H */
