/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\input.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, July 6th 2019, 6:23:40 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "input.h"

static inputs _current, _last;

static void INPT_Process(inputs _in)
{
    memset(&_current, 0, sizeof(inputs));

    /* Handle Setting Single press */

    if (_in.btn_a && !_last.btn_a)
    {
        _current.btn_a |= BTN_PRESS;
    }
    if ((_in.btn_a) && (_last.btn_a))
    {
        _current.btn_a |= BTN_HELD;
    }
    if ((!_in.btn_a) && (_last.btn_a))
    {
        _current.btn_a |= BTN_RELEASE;
    }
    if (_in.btn_b && !_last.btn_b)
    {
        _current.btn_b |= BTN_PRESS;
    }
    if ((_in.btn_b) && (_last.btn_b))
    {
        _current.btn_b |= BTN_HELD;
    }
    if ((!_in.btn_b) && (_last.btn_b))
    {
        _current.btn_b |= BTN_RELEASE;
    }
    if (_in.btn_x && !_last.btn_x)
    {
        _current.btn_x = 1;
    }
    if (_in.btn_y && !_last.btn_y)
    {
        _current.btn_y = 1;
    }
    if (_in.btn_start && !_last.btn_start)
    {
        _current.btn_start = 1;
    }

    /* Handle DPAD Values */

    if ((_in.dpad & (1 << DPAD_DOWN)) && !(_last.dpad & (1 << DPAD_DOWN)))
    {
        _current.dpad |= (1 << DPAD_DOWN);
    }

    if ((_in.dpad & (1 << DPAD_DOWN)) && (_last.dpad & (1 << DPAD_DOWN)))
    {
        _current.dpad |= (1 << DPAD_DOWN_HELD);
    }

    if ((_in.dpad & (1 << DPAD_UP)) && !(_last.dpad & (1 << DPAD_UP)))
    {
        _current.dpad |= (1 << DPAD_UP);
    }

    if ((_in.dpad & (1 << DPAD_UP)) && (_last.dpad & (1 << DPAD_UP)))
    {
        _current.dpad |= (1 << DPAD_UP_HELD);
    }

    if ((_in.dpad & (1 << DPAD_LEFT)) && !(_last.dpad & (1 << DPAD_LEFT)))
    {
        _current.dpad |= (1 << DPAD_LEFT);
    }

    if ((_in.dpad & (1 << DPAD_LEFT)) && (_last.dpad & (1 << DPAD_LEFT)))
    {
        _current.dpad |= (1 << DPAD_LEFT_HELD);
    }

    if ((_in.dpad & (1 << DPAD_RIGHT)) && !(_last.dpad & (1 << DPAD_RIGHT)))
    {
        _current.dpad |= (1 << DPAD_RIGHT);
    }

    if ((_in.dpad & (1 << DPAD_RIGHT)) && (_last.dpad & (1 << DPAD_RIGHT)))
    {
        _current.dpad |= (1 << DPAD_RIGHT_HELD);
    }

    _last = _in;
}

void INPT_ReceiveFromHost(inputs _in)
{
    INPT_Process(_in);
}

bool INPT_Button(BUTTON btn)
{
    switch (btn)
    {
    case BTN_A:
        return _current.btn_a;
        break;
    case BTN_B:
        return _current.btn_b;
        break;
    case BTN_X:
        return _current.btn_x;
        break;
    case BTN_Y:
        return _current.btn_y;
        break;
    case BTN_START:
        return _current.btn_start;
        break;
    default:
        break;
    }
    return false;
}
bool INPT_ButtonEx(BUTTON btn, ACTION_TYPE type)
{
    switch (btn)
    {
    case BTN_A:
        return (_current.btn_a & type) == type;
        break;
    case BTN_B:
        return (_current.btn_b & type) == type;
        break;
    case BTN_X:
        return (_current.btn_x & type) == type;
        break;
    case BTN_Y:
        return (_current.btn_y & type) == type;
        break;
    case BTN_START:
        return _current.btn_start == type;
        break;
    default:
        break;
    }
    return false;
}

dpad_t INPT_DPAD()
{
    return _current.dpad;
}

bool INPT_DPADDirection(DPAD_DIRECTION dir)
{
    return (_current.dpad & (1 << dir));
}