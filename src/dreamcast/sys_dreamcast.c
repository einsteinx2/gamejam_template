#include <kos.h>

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <limits.h>
#include <arch/arch.h>

#include <dc/maple.h>
#include <dc/maple/controller.h>
#include <assert.h>

#include "errno.h"

#include "common.h"
#include "renderer.h"
#include "input.h"

#if _arch_dreamcast
//extern uint8 romdisk[];
//KOS_INIT_ROMDISK(romdisk);
#endif

static void drawtext(int x, int y, char *string);
void Sys_Error(char *error, ...);
void Sys_Quit(void);

extern void Host_Frame(float time);

// settings
const unsigned int SCR_WIDTH = 640;
const unsigned int SCR_HEIGHT = 480;

/*
===============================================================================

FILE IO

===============================================================================
*/

#define MAX_HANDLES 10
FILE *sys_handles[MAX_HANDLES];

int findhandle(void)
{
	int i;

	for (i = 1; i < MAX_HANDLES; i++)
		if (!sys_handles[i])
			return i;
	Sys_Error("out of handles");
	return -1;
}

int Sys_FileOpenRead(char *path, int *hndl)
{
	FILE *f;
	int i;

	i = findhandle();
	if (i == -1)
	{
		return -1;
	}

	f = fopen(path, "r");
	if (!f)
	{
		printf("[%s] Error opening %s: %s\n", __func__, path, strerror(errno));
		*hndl = -1;
		return -1;
	}
	sys_handles[i] = f;
	*hndl = i;

	return Sys_FileLength(f);
}

int Sys_FileOpenWrite(char *path)
{
	FILE *f;
	int i;

	i = findhandle();

	f = fopen(path, "w");
	if (f == NULL)
		Sys_Error("[%s] Error opening %s: %s", __func__, path, strerror(errno));
	sys_handles[i] = f;

	return i;
}

void Sys_FileClose(int handle)
{
	fclose(sys_handles[handle]);
	sys_handles[handle] = NULL;
}

void Sys_FileSeek(int handle, int position)
{
	fseek(sys_handles[handle], position, SEEK_SET);
}

int Sys_FileRead(int handle, void *dest, int count)
{
	int x;
	x = fread(dest, 1, count, sys_handles[handle]);
	if (x == -1)
	{
		printf("%s: ERRO!\n", __func__);
	}
	return x;
}

int Sys_FileWrite(int handle, void *data, int count)
{
	int x;
	x = fwrite(data, 1, count, sys_handles[handle]);
	if (x == -1)
	{
		//printf("%s: ERRO!\n",__func__);
	}
	return x;
}

int Sys_FileTime(char *path)
{
	struct stat buffer;
	return ((stat(path, &buffer) == 0) ? 1 : -1);
}

void Sys_mkdir(__attribute__((unused)) char *path)
{
}

/*
===============================================================================

SYSTEM IO

===============================================================================
*/
void Sys_Error(char *error, ...)
{
	va_list argptr;

	printf("Sys_Error: ");
	va_start(argptr, error);
	vprintf(error, argptr);
	va_end(argptr);
	printf("\n");

	Sys_Quit();
}

void Sys_Printf(char *fmt, ...)
{
	va_list argptr;

	va_start(argptr, fmt);
	vprintf(fmt, argptr);
	va_end(argptr);
}

void Sys_Quit(void)
{
	arch_exit();
#if 0
	glKosSwapBuffers();
	Host_Shutdown();
	vid_set_mode(DM_640x480, PM_RGB565);
	vid_empty();

	/* Display the error message on screen */
	drawtext(32, 64, "nuQuake shutdown...");
	arch_menu();
#endif
}

#include <sys/time.h>

double Sys_FloatTime(void)
{
	struct timeval tp;
	struct timezone tzp;
	static int secbase;

	gettimeofday(&tp, &tzp);

	if (!secbase)
	{
		secbase = tp.tv_sec;
		return tp.tv_usec / 1000000.0;
	}

	return (tp.tv_sec - secbase) + tp.tv_usec / 1000000.0;
}

void Sys_Sleep(void)
{
}

//-----------------------------------------------------------------------------
static void drawtext(int x, int y, char *string)
{
	printf("%s\n", string);
	int offset = ((y * 640) + x);
	bfont_draw_str(vram_s + offset, 640, 1, string);
}

static void assert_hnd(const char *file, int line, const char *expr, const char *msg, const char *func)
{
	char strbuffer[1024];

	/* Reset video mode, clear screen */
	vid_set_mode(DM_640x480, PM_RGB565);
	vid_empty();

	/* Display the error message on screen */
	drawtext(32, 64, "nuQuake - Assertion failure");

	sprintf(strbuffer, " Location: %s, line %d (%s)", file, line, func);
	drawtext(32, 96, strbuffer);

	sprintf(strbuffer, "Assertion: %s", expr);
	drawtext(32, 128, strbuffer);

	sprintf(strbuffer, "  Message: %s", msg);
	drawtext(32, 160, strbuffer);

	/* Hang - infinite loop */
	while (1)
		;
}

// process all input: the handler will figure out which are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput()
{
	static inputs _input;
	int buttons;

	maple_device_t *cont;
	cont_state_t *state;

	cont = maple_enum_type(0, MAPLE_FUNC_CONTROLLER);
	if (!cont)
		return;
	state = (cont_state_t *)maple_dev_status(cont);

	buttons = state->buttons;
	//if (state->ltrig>0) buttons|=(1<<16);
	//if (state->rtrig>0) buttons|=(1<<17);

	/*  Reset Everything */
	memset(&_input, 0, sizeof(inputs));

	/* DPAD */
	_input.dpad = (state->buttons >> 4);

	/* BUTTONS */
	_input.btn_a = (uint8_t)(buttons & CONT_A);
	_input.btn_b = (uint8_t)(buttons & CONT_B);
	_input.btn_x = (uint8_t)(buttons & CONT_X);
	_input.btn_y = (uint8_t)(buttons & CONT_Y);
	_input.btn_start = (uint8_t)(buttons & CONT_START);

	INPT_ReceiveFromHost(_input);
}

int main(int argc, char **argv)
{
	// Set up assertion handler
	assert_set_handler(assert_hnd);

	GLdcConfig config;
	glKosInitConfig(&config);

	config.initial_op_capacity = 8096;
	config.initial_pt_capacity = 2048;
	config.initial_tr_capacity = 4096;
	config.initial_immediate_capacity = 4096;

	glKosInitEx(&config);
	double time, oldtime, newtime;

	oldtime = Sys_FloatTime() - 0.1;
	//profiler_enable();
	Game_Main(argc, argv);
	while (1)
	{
		newtime = Sys_FloatTime();
		time = newtime - oldtime;
		processInput();
		//Handle Input in Game
		Host_Input(time);

		Host_Frame(time);
		glKosSwapBuffers();
		oldtime = newtime;
	}
	return 1;
}