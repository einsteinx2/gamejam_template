#include "common.h"
#include "renderer.h"
#include "input.h"

#include <stdio.h>
#include <stdbool.h>

void Host_Frame(float time);

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 640;
const unsigned int SCR_HEIGHT = 480;

double Sys_FloatTime(void)
{
	return glfwGetTime();
}

int main(int argc, char **argv)
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "GameJam Game", NULL, NULL);
    if (window == NULL)
    {
        printf("Failed to create GLFW window\n");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        printf("Failed to initialize GLAD\n");
        return -1;
    }    

    float time, oldtime, newtime;

    oldtime = Sys_FloatTime() - 0.1;
    Game_Main(argc, argv);
    

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        newtime = Sys_FloatTime();
		time = newtime - oldtime;
        // input
        // -----
        processInput(window);

        // Handle Input in Game
        Host_Input(time);
        
        // Render
        Host_Frame(time);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
        oldtime = newtime;
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

static inputs _input;

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    /*  Reset Everything */
    memset(&_input,0,sizeof(inputs));
    
    /* Mark out DPAD inputs */
    if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
        _input.dpad |= (1 << DPAD_LEFT);
    }
    if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
        _input.dpad |= (1 << DPAD_RIGHT);
    }
    if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
        _input.dpad |= (1 << DPAD_DOWN);
    }
    if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
        _input.dpad |= (1 << DPAD_UP);
    }

    /* Mark out Buttons */
    if(glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS){
        _input.btn_a = 1;
    }
    if(glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS){
        _input.btn_b = 1;
    }
    if(glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS){
        _input.btn_x = 1;
    }
    if(glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS){
       _input.btn_y = 1;
    }
    if(glfwGetKey(window, GLFW_KEY_SLASH) == GLFW_PRESS){
       _input.btn_start = 1;
    }

    INPT_ReceiveFromHost(_input);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    (void)window;
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}