
default: help
all : help

help:
	@echo " \
	                         __  \n\
	 _____ _____ __    _____|  | \n\
	|  |  |   __|  |  |  _  |  | \n\
	|     |   __|  |__|   __|__| \n\
	|__|__|_____|_____|__|  |__| \n\
	========================================"
	@echo " \
	Building:							    \n\
		dc\t\tSega Dreamcast (elf, bin)		\n\
		psp\t\tSony PSP (prx, cfw)			\n\
		win\t\tWindows (exe, mingw64)		\n\
		build-all\tBuilds each platform     \n\
		\n\
	Clean:									\n\
		clean-dc\t\t						\n\
		clean-psp\t\t						\n\
		clean-win\t\t						\n\
		clean-all\t\t					    \n\
		\n\
	Testing:										\n\
		test-dc\t\tBuild cdi and launch NullDC		\n\
		test-psp\tBuild EBOOT.PBP and launch PPSSPP\n\
		test-win\tRun .exe							\n\
		test-all\tRuns all Three  					\n\
	"

dc:
	@echo " \
	                                          \n\
	 ____                                _    \n\
	|    \ ___ ___ ___ _____ ___ ___ ___| |_  \n\
	|  |  |  _| -_| .'|     |  _| .'|_ -|  _| \n\
	|____/|_| |___|__,|_|_|_|___|__,|___|_|   \n\
	========================================"
	@docker run --rm -v "${PWD}:/src"  haydenkow/nu-dcdev:release-4.7.3-rc3 make -j5 -f Makefile.dc

win:
	@echo " \
	                                \n\
	 _ _ _ _       _                \n\
	| | | |_|___ _| |___ _ _ _ ___  \n\
	| | | | |   | . | . | | | |_ -| \n\
	|_____|_|_|_|___|___|_____|___| \n\
	========================================"
	@docker run --rm -v "${PWD}:/src"  -w "/src" kazade/windows-sdk mingw64-make -j5 -f Makefile.win  

psp:
	@echo " \
	                    \n\
	 _____ _____ _____  \n\
	|  _  |   __|  _  | \n\
	|   __|__   |   __| \n\
	|__|  |_____|__|    \n\
	========================================"
	@docker run --rm -v "${PWD}:/src" haydenkow/nu-pspdev make -s -k -j5 -f Makefile.psp

# clean house
clean-dc:
	rm -rf build_dc/src/*

# clean house
clean-win:
	rm -rf build_win/src/*

# clean house
clean-psp:
	rm -rf build_psp/src/*

test-dc: dc
	cp build_dc/1ST_READ.BIN build_cdi/data_hb/
	cmd.exe /C "cd build_cdi && (build_image.bat > nul 2>&1) && run_image.bat  "

test-win: win
	cmd.exe /C "cd D:\Dev\Dreamcast\UB_SHARE\gamejam\game\build_win && ""D:\Dev\TDM-GCC-64\bin\gdb.exe"" -q -command ..\debug-win.txt  ""D:\Dev\Dreamcast\UB_SHARE\gamejam\game\build_win\gamejam.exe"" "

test-psp: psp
	cp build_psp/EBOOT.PBP build_pbp/
	cmd.exe /C "cd build_pbp  && run_image.bat  "

clean-all: clean-dc clean-win clean-psp

build-all: dc win psp

test-all: test-dc test-psp test-win

%.sh:

%: %.sh
